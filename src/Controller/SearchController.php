<?php

namespace App\Controller;

use App\Entity\Book;
use App\Repository\BookRepository;
use App\Repository\SearchRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/search')]
class SearchController extends AbstractController
{
    private $books;

    #[Route('/', name: 'search_index', methods: ['GET'])]
    public function index(Request $request, SearchRepository $searchRepository): Response
    {
        $page=1;
        $tmp = $request->query->get('page');
        if(!empty($tmp)){
            $page = $tmp;
        }
        $result = $searchRepository->searchList($request, $page); //full, req, num, books
        $count = $searchRepository->getCount();
        $maxPage = 1;
        if( $count !== 0){
            $maxPage = ceil( $count/ $result[2]);
        }
        $thisPage = $page;
        return $this->renderForm('search/index.html.twig', [
            'books' => $result[3],
            'full' => $result[0],
            'request' => $result[1],
            'num' => $result[2],
            'maxPages' => $maxPage,
            'thisPage' => $thisPage
        ]);

    }
}
