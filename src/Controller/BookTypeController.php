<?php

namespace App\Controller;

use App\Entity\BookType;
use App\Form\BookTypeType;
use App\Repository\BookTypeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 *
 */
#[Route('/book_type')]
class BookTypeController extends AbstractController
{
    /**
     * @param BookTypeRepository $bookTypeRepository
     * @return Response
     */
    #[Route('/', name: 'book_type_index', methods: ['GET'])]
    public function index(BookTypeRepository $bookTypeRepository): Response
    {
        return $this->render('book_type/index.html.twig', [
            'book_types' => $bookTypeRepository->findAll(),
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    #[Route('/new', name: 'book_type_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $bookType = new BookType();
        $form = $this->createForm(BookTypeType::class, $bookType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($bookType);
            $entityManager->flush();

            return $this->redirectToRoute('book_type_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('book_type/new.html.twig', [
            'book_type' => $bookType,
            'form' => $form,
        ]);
    }

    /**
     * @param BookType $bookType
     * @return Response
     */
    #[Route('/{id}', name: 'book_type_show', methods: ['GET'])]
    public function show(BookType $bookType): Response
    {
        return $this->render('book_type/show.html.twig', [
            'book_type' => $bookType,
        ]);
    }

    /**
     * @param Request $request
     * @param BookType $bookType
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    #[Route('/{id}/edit', name: 'book_type_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, BookType $bookType, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(BookTypeType::class, $bookType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('book_type_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('book_type/edit.html.twig', [
            'book_type' => $bookType,
            'form' => $form,
        ]);
    }

    /**
     * @param Request $request
     * @param BookType $bookType
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    #[Route('/{id}', name: 'book_type_delete', methods: ['POST'])]
    public function delete(Request $request, BookType $bookType, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$bookType->getId(), $request->request->get('_token'))) {
            //Added onCascade:Set NULL
            $entityManager->remove($bookType);
            $entityManager->flush();
        }

        return $this->redirectToRoute('book_type_index', [], Response::HTTP_SEE_OTHER);
    }
}
