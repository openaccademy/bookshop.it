<?php

namespace App\Command;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\DocBlock\Tags\Author;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity;
use Faker\Factory;
use function Symfony\Component\Translation\t;

#[AsCommand(
    name: 'FillDBCommand',
    description: 'Faker to fill db',
)]
class FillDBCommand extends Command
{
    protected $log;
    protected $do;
    
    public function __construct(EntityManagerInterface $do, LoggerInterface $log)
    {
        $this->do = $do;
        $this->log = $log;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addOption('entity', 'en', InputOption::VALUE_REQUIRED, 'Number of entities you want to create');
    }

    protected function persist($entity) : void {
        $this->do->persist($entity);
    }

    protected function createNewBook($title, $num_pages, $plot, $publish_date, $price, $authorID, $typeID) : Entity\Book {
        $tmp = new Entity\Book();
        $tmp->setTitle($title);
        $tmp->setNumPages($num_pages);
        $tmp->setPlot($plot);
        $tmp->setPublishDate($publish_date);
        $tmp->setPrice($price);
        $tmp->setAuthor($authorID);
        $tmp->setType($typeID);
        $this->persist($tmp);
        return $tmp;
    }
    protected function createNewBookType($category) : Entity\BookType{
        $tmp = new Entity\BookType();
        $tmp->setCategory($category);
        $this->persist($tmp);
        return $tmp;
    }
    protected function createNewAuthor($name, $surname, $born_date, $avatar) : Entity\Author{
        $tmp = new Entity\Author();
        $tmp->setName($name);
        $tmp->setSurname($surname);
        $tmp->setBornDate($born_date);
        $tmp->setAvatar($avatar);
        $this->persist($tmp);
        return $tmp;
    }
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $numen = $input->getOption('entity');
        if ($numen) {
            $io->info('Starting population of the DB');
            $faker = Factory::create();
            for($i = 0; $i < $numen; ++$i){
                $this->createNewBook(
                    $faker->word,
                    $faker->randomDigit(),
                    $faker->text(),
                    $faker->dateTime(),
                    $faker->randomDigit(),
                    $this->createNewAuthor($faker->firstName, $faker->lastName, $faker->dateTime(), 'prova avatar'),
                    $this->createNewBookType($faker->word())
                );
            }
            $this->do->flush();
            $io->success('Database population went successful!');
        }
        else{
            $io->error('You have to give the number of entities that you want to create!.');
            return Command::FAILURE;
        }
        return Command::SUCCESS;
    }
}
