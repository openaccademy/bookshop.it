<?php

namespace App\Repository;

use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use http\Env\Request;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;


class SearchRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */


    private $do;
    private $count = 1;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $do)
    {
        parent::__construct($registry, Book::class);
        $this->do = $do;
    }

    public function searchList(\Symfony\Component\HttpFoundation\Request $request, $currentPage) {
        $full = false;
        $req = $request->query->get('_search');
        if (!empty($req)) {
            $num = $request->query->get('_num');
            $full = true;
            //$query = $do->createQuery('SELECT u FROM  App\Entity\Book u WHERE u.title LIKE "%?1%"')
            $query = $this->do->createQuery("SELECT u FROM  App\Entity\Book u WHERE u.title LIKE ?1")
                ->setParameter(1, '%' . $req . '%');
            $tmp = $query->getResult();
            $books = $this->paginate($num, $query, $currentPage);
            return [$full, $req, $num, $books];
        }
        return [false, ' ', 1, [] ];

    }
    public function paginate($limit, $dql, $page = 1)
    {
        $paginator = new Paginator($dql);

        $paginator->getQuery()
            ->setFirstResult($limit * ($page - 1)) // Offset
            ->setMaxResults($limit); // Limit
        $this->count = $paginator->count();

        return $paginator;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }





}
