#*Scaffolding*
> ##Description 
- This is a Symfony project. The Symfony version used for the development is the *5.3.10*.
- The PHP minimum version required in order to run the project is *7.2.5*.
- This project use Doctrine as an ORM to persist entities in a MYSQL database. 
- Talking about the front-end part, this project use bootstrap as an HTML and CSS framework.

---

> ##Useful tips
- Check your version of php: you need php 7.2.5 or higher (be careful because php cli and php used by the web server **are not the same things**, you have to check the version related to the web server).
- This project use Boostrap, to ensure the better quality of this project is required a minimal version of node such as *16.13.0*.  
  The project contain a *.nvmrc* file, so if you have already added the related script to your .bashrc (or .othershellrc) file, you should not come across problem with node versions.
 ---
> ##Instructions
- Clone the Repository from the [Bitbucket repository](https://bitbucket.org/openaccademy/bookshop.it/src/develop/)
- After the cloning of the BitBucket Repository, copy the "*.env.dist*" file and rename it as "*.env.local*". After that edit the "*.env.local*" file with your data in order to ensure to Doctrine the correct access to the database.
- From the shell, execute "**composer install**" to get all the modules used for the development.  
- Install a web server in your system. Then edit its own configuration: You have to point the root page of the webserver to the *public* directory of this project.
- As a reminder check the Node version : it has to be *16.13.0* or higher. 

 



