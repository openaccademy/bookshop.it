#**BOOKSHOP**

>##Description:
This bookshop gives you the chance of storing and viewing all the information that you need about books and authors.
To store all the data, this system use a MySql database. The bookshop use an ORM (Doctrine) to engage with the database.
---
>##Functionalities:
- **Add new books:**  
    With the help of a specific form you can add new books to the bookshops. Before you should have added to the bookshop the book-type and the author of the book you want to insert. <br><br>
- **Add new authors:**  
    You can add new authors with the use of a specific form.   <br><br>
- **Add new book-types:**  
    You can add new book-type with the use of a specific form. <br><br>
- **Edit books stored:**  
    Using a form you will be able to modify all the data of books that you have added previously. <br><br>
- **Edit authors stored:**  
    With the help of a specific form you can modify author to the bookshops. Before you should have registered the book-type and the author of the book that you are adding. <br><br>
- **Edit book-types stored:**  
    With the help of a specific form you can modify author to the bookshops. Before you should have registered the book-type and the author of the book that you are adding. <br><br>
- **Delete added books:**   
    Consulting your collections you can delete the books you want to.  <br><br>
- **Delete added authors:**  
    Consulting your collections you can delete the authors you want to.  <br><br>
- **Delete added book-types:**  
    Consulting your collections you can delete the book-types you want to.
 