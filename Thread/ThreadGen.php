<?php

namespace App\Thread;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\DocBlock\Tags\Author;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity;
use Faker\Factory;
use function Symfony\Component\Translation\t;


class ThreadGen extends \Thread  {
    protected $log;
    protected $do;
    protected $num;
    public function __construct(EntityManagerInterface $do, LoggerInterface $log){
        $this->do = $do;
        $this->log= $log;
    }

    /**
     * @param mixed $num
     */
    public function setNum($num): void
    {
        $this->num = $num;
    }

    /**
     * @return mixed
     */
    public function getNum()
    {
        return $this->num;
    }


    protected function persist($entity) : void {
        $this->do->persist($entity);
        $this->do->flush();
    }

    protected function createNewBook($title, $num_pages, $plot, $publish_date, $price, $authorID, $typeID) : Entity\Book {
        $tmp = new Entity\Book();
        $tmp->setTitle($title);
        $tmp->setNumPages($num_pages);
        $tmp->setPlot($plot);
        $tmp->setPublishDate($publish_date);
        $tmp->setPrice($price);
        $tmp->setAuthor($authorID);
        $tmp->setType($typeID);
        $this->persist($tmp);
        return $tmp;
    }

    protected function createNewBookType($category) : Entity\BookType{
        $tmp = new Entity\BookType();
        $tmp->setCategory($category);
        $this->persist($tmp);
        return $tmp;
    }
    protected function createNewAuthor($name, $surname, $born_date, $avatar) : Entity\Author{
        $tmp = new Entity\Author();
        $tmp->setName($name);
        $tmp->setSurname($surname);
        $tmp->setBornDate($born_date);
        $tmp->setAvatar($avatar);
        $this->persist($tmp);
        return $tmp;
    }
    public function run(){
        $faker = Factory::create();
        for($i = 0; $i < $this->num; ++$i){
            $this->createNewBook(
                $faker->word,
                $faker->randomDigit(),
                $faker->text(),
                $faker->dateTime(),
                $faker->randomDigit(),
                $this->createNewAuthor($faker->firstName, $faker->lastName, $faker->dateTime(), 'prova avatar'),
                $this->createNewBookType($faker->word())
            );
        }
    }
}

